﻿
# Flappy Bird sous Unity

Le projet Unity s'est porté sur le clone d'un Flappy Bird.

## Le travail fourni

Le jeu est opérationnel, avec 3 scènes différentes (Intro, le jeu en lui-même et la scène de fin).

## Problèmes rencontrés

La plus grosse difficulté rencontrée était la rotation de l'oiseau jusqu'à ce qu'il touche le sol,
le problème fut résolu en créant une variable ground qui permettait de savoir à quel moment le jeu devait passer
à la scène suivante.
Un autre de mes problème était que le jeu ne voulait pas compiler pour un web player. Le souci était que le dossier
source se trouvait dans un nom d'utilisateur comportant un caractère incorrect pour le compilateur. Le souci fut réglé
en changeant l'emplacement des sources du jeu.
Un autre souci rencontré est que malgré le fait que le jeu soit forcé à être d'une résolution de 640x1136 pour PC, le jeu
se lance tout de même en mode paysage et du coup rend beaucoup moins bien qu'en verison paysage.

## Support testés

Le jeu fut testé sur Mozilla Firefox. Il fut aussi testé sur mon ordinateur personnel. Pour la version Android, le jeu fut
testé sur un Honor 5X, un Nexus 4 ainsi qu'un Samsung Galaxy J3. Parmi tout les devices présentés aucuns ne supportaient pas le jeu.

## Ajouts au jeu

Les ajouts furent une mécanique d'augmentation du défilement des tuyaux tout les 5 points obtenus, pour pimenter un peu le jeu.
J'ai aussi pris l'initiative de créer 3 musiques en 8-bit pour chaque scène du jeu. Elles correspondent à ce que j'imaginais pour chaque
scène, que ce soit l'intro un peu mystérieuse, la musique de jeu qui est assez répétitive mais tout de même entrainante et rappelera l'époque
Nintendo contre Sega. La musique d'outro quand à elle indique bien la défaite du joueur.

## Conclusion

Ce fut un projet très intéréssant et très motivant. On peut voir la puissance de l'outil qu'est Unity même sur un jeu aussi simple.
Le travail sur Unity restera pour moi une expérience enrichissante.

## Rémy Rauflet LP2B
