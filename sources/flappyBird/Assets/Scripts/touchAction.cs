﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class touchAction : MonoBehaviour {

	public Vector2 speed;
	public Vector2 movement;
	private AudioSource source;

	// Use this for initialization
	void Start () {   
		source = GameObject.Find("flapflap").GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown(0)) {
			source.Play();
			movement = new Vector2 (0f, speed.y * 5);
			GetComponent<Rigidbody2D> ().velocity = movement;
		} else if (Input.touchCount > 0){
			source.Play();
			Touch myTouch = Input.GetTouch (0);
			Touch[] myTouches = Input.touches;
			for (int i = 0; i < Input.touchCount; i++) {
				movement = new Vector2 (0f, speed.y * 5);
				GetComponent<Rigidbody2D> ().velocity = movement;
			}
		}
	}
}

