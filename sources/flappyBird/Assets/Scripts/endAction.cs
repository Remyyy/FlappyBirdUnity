﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endAction : MonoBehaviour {

	float speed = 5f;
	bool isDead = false;
	bool ground = false;
	Vector3 botLeft;
	float yAxis;

	// Use this for initialization
	void Start () {
		botLeft = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));

	}
	
	// Update is called once per frame
	public void Update () {
		yAxis = GameObject.Find("bird").GetComponent<SpriteRenderer> ().transform.position.y;
		if (isDead && !ground){
			GameObject.Find ("bird").GetComponent<Rigidbody2D>().transform.Rotate (0, 0, speed);
			if(yAxis < -3){
				ground = true;
				SceneManager.LoadScene("scene4-End");
			}
		}
	}

	public void endGame (){
		isDead = true;
		Destroy (GameObject.Find ("bird").GetComponent<Animator> ());
		GameObject.Find ("pipePair1").GetComponent<movePipes> ().movement = new Vector2(0,0);
		GameObject.Find ("pipePair2").GetComponent<movePipes> ().movement = new Vector2(0,0);

	}
}
