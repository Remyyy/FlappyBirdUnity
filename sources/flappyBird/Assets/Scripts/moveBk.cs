﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBk : MonoBehaviour {

	public float positionRestartX = 10f;
	private Vector3 siz;
	private Vector2 movement;
	private Vector3 leftBottomCameraBorder;

	// Use this for initialization
	void Start () {
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		movement = new Vector2 (-0.5f, 0);
	}

	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D>().velocity = movement;
		siz.x= gameObject.GetComponent<SpriteRenderer>().bounds.size.x; 
		siz.y= gameObject.GetComponent<SpriteRenderer>().bounds.size.y; 

		if (transform.position.x < leftBottomCameraBorder.x -(siz.x/2)) 
		{ 
			transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z); 
		} 
	}
}
