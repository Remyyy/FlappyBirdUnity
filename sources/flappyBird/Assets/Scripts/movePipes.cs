﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour {


	public Vector3 siz;
	public Vector2 movement;
	public Vector3 leftBottomCameraBorder;
	public Vector3 rightBottomCameraBorder;
	public GameObject upPipe1;
	public GameObject downPipe1;
	private Transform upPipe1OriginalTransform;
	private Transform downPipe1OriginalTransform;
	public GameObject score;
	private Transform scoreOriginalTransform;

	// Use this for initialization
	void Start () {
		scoreOriginalTransform = score.transform;
		upPipe1OriginalTransform = upPipe1.transform;
		downPipe1OriginalTransform = downPipe1.transform;
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, 0));
	}
	
	// Update is called once per frame
	void Update(){	
		if (GlobalControl.Instance.score % 5 == 0 && GlobalControl.Instance.score != 0) {
			float tmp = movement.x - 0.01f;
			movement = new Vector2(tmp,0f);
		}
		upPipe1.GetComponent<Rigidbody2D>().velocity = movement;
		downPipe1.GetComponent<Rigidbody2D>().velocity = movement;
		score.GetComponent<Rigidbody2D> ().velocity = movement;
		siz.x = upPipe1.GetComponent<SpriteRenderer> ().bounds.size.x;
		siz.y = downPipe1.GetComponent<SpriteRenderer> ().bounds.size.y;
		if (upPipe1.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2))
			moveToRightPipe ();
	}

	void moveToRightPipe(){
		float randomY = Random.Range (1, 4) - 2;
		float posX = rightBottomCameraBorder.x + (siz.x / 2);
		float posY = upPipe1OriginalTransform.position.y + randomY ;
		Vector3 tmpPos = new Vector3 (posX, posY, upPipe1.transform.position.z);
		upPipe1.transform.position = tmpPos;

		posY = downPipe1OriginalTransform.position.y + randomY ;
		tmpPos = new Vector3 (posX, posY, downPipe1.transform.position.z);
		downPipe1.transform.position = tmpPos;

		posY = scoreOriginalTransform.position.y + randomY ;
		tmpPos = new Vector3 (posX, posY, score.transform.position.z);
		score.transform.position = tmpPos;
	}
}