﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posBird : MonoBehaviour {

	Vector3 siz;
	Vector3 topLeft;
	Vector3 botLeft;
	Vector3 topRight;
	Vector3 botRight;

	// Use this for initialization
	void Start () {
		topLeft = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, 0));
		botLeft = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		topRight = Camera.main.ViewportToWorldPoint (new Vector3 (1, 1, 0));
		botRight = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, 0));
	}

	// Update is called once per frame
	void Update () {
		siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

		gameObject.transform.position = new Vector3 (topLeft.x + (siz.x / 2), transform.position.y, transform.position.z);

		if(transform.position.y < botLeft.y + (siz.y / 2))
			gameObject.transform.position = new Vector3 (transform.position.x, botLeft.y + (siz.y / 2), transform.position.z);

		if(transform.position.y > topLeft.y - (siz.y / 2))
			gameObject.transform.position = new Vector3 (transform.position.x, topLeft.y - (siz.y / 2), transform.position.z);
	}
}