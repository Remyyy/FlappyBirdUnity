﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideManagementBird : MonoBehaviour {

	int score=0;

	// Use this for initialization
	void Start () {
		score = 0;
		GlobalControl.Instance.score = score;
	}
	
	// Update is called once per frame
	void Update () {
		GameObject.Find ("Text").GetComponent<UnityEngine.UI.Text>().text = "" + score;
	}


	void OnTriggerEnter2D (Collider2D collider){
		if (collider.name.Equals("score1") || collider.name.Equals("score2")) {
			score ++;
			GlobalControl.Instance.score++;
		} else {
			Destroy(GetComponent<touchAction>());
			GetComponent<endAction> ().endGame ();
		}
	}
}
