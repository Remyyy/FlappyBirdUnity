﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class rePlay : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject.Find ("Text").GetComponent<UnityEngine.UI.Text>().text = "" + GlobalControl.Instance.score;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown (0) || Input.touchCount > 0) {
			SceneManager.LoadScene("scene3-Game");
		} 
	}
}
